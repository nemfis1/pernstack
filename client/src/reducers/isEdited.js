const showMessage = (state = false, action) => {
    switch(action.type) {
        case 'EDITED':
            return !state;
       default:
            return state;
    }
}
export default showMessage;