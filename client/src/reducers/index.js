import counterReducer from './counter';
import showMessage from './isEdited';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    counter: counterReducer,
    message: showMessage
});

export default allReducers;