import React, { Fragment } from "react";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import * as action from "./actions";

//components

import InputTodo from "./components/InputTodo";
import ListTodos from "./components/ListTodos";

function App() {
  const brojac = useSelector(State => State.counter);
  const dispatch = useDispatch();
  return (
    <Fragment>
      <div className="container">
        <InputTodo />
        <ListTodos />
        <h1>Brojac : {brojac}</h1>
        <button onClick={()=>dispatch(action.increment())}>+</button>
        <button onClick={()=>dispatch(action.decrement())}>-</button>
      </div>
    </Fragment>
  );
}

export default App;
